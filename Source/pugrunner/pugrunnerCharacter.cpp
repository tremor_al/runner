// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "pugrunnerCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectIterator.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Public/TimerManager.h"

//////////////////////////////////////////////////////////////////////////
// ApugrunnerCharacter

ApugrunnerCharacter::ApugrunnerCharacter()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input
void ApugrunnerCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = Cast<APlayerController>(Controller);
}
// Called every frame
void ApugrunnerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveForward(VelocityMovement);
	//}
	//SwipeLeft();
	if (ScreenPressed)
	{
		FVector2D TouchCurrent2DVector;
		bool h;
		PlayerController->GetInputTouchState(ETouchIndex::Touch1, TouchCurrent2DVector.X, TouchCurrent2DVector.Y, h);
		float s = FMath::Sqrt(TouchStart2DVector.X*TouchCurrent2DVector.X + TouchStart2DVector.Y*TouchCurrent2DVector.Y);
		if (s > 100.0f)
		{
			ScreenPressed = false;
		}
	}

	//HorizontalObstacleTrace();

	if (strife_right == true)
	{
		Strafe(VelocityMovement);
	}
	if (strife_left == true)
	{
		Strafe(-1 * VelocityMovement);
	}
}

void ApugrunnerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ApugrunnerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ApugrunnerCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ApugrunnerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ApugrunnerCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ApugrunnerCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ApugrunnerCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ApugrunnerCharacter::OnResetVR);
}

void ApugrunnerCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ApugrunnerCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	//Jump();
	bool h;
	PlayerController->GetInputTouchState(FingerIndex, TouchStart2DVector.X, TouchStart2DVector.Y, h);
	ScreenPressed = true;
	//UE_LOG(LogScript, Warning, TEXT("ScreenLoc Dep %f %f Pressed %f"), TouchStart2DVector.X, TouchStart2DVector.Y, ScreenPressed);
}

void ApugrunnerCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	//StopJumping();
	ScreenPressed = false;

	bool h;
	PlayerController->GetInputTouchState(FingerIndex, TouchEnd2DVector.X, TouchEnd2DVector.Y, h);
	FVector2D lengthAbs(FMath::Abs(TouchStart2DVector.X - TouchEnd2DVector.X), FMath::Abs(TouchStart2DVector.Y - TouchEnd2DVector.Y));
	FVector2D length(TouchStart2DVector.X - TouchEnd2DVector.X, TouchStart2DVector.Y - TouchEnd2DVector.Y);
	if (lengthAbs.X > lengthAbs.Y)
	{
		//left right
		if (length.X > 0)
		{
			UE_LOG(LogScript, Warning, TEXT("LEFT"));
			//strife_left = true;
			SwipeLeft();
			start = FPlatformTime::Seconds();

		}
		else
		{
			UE_LOG(LogScript, Warning, TEXT("RIGHT"));
			//strife_right = true;
			SwipeRight();
			start = FPlatformTime::Seconds();
		}

	}
	else
	{
		//up down
		if (length.Y > 0)
		{
			UE_LOG(LogScript, Warning, TEXT("UP"));
			SwipeJump();
		}
		else if (length.Y < 0)
		{
			UE_LOG(LogScript, Warning, TEXT("DOWN"));
			SwipeCrouch();
		}
	}
}

void ApugrunnerCharacter::Strafe(float Value)
{
	float LastIndex = 0;
	float NumObjectsPerTick = 500;
	for (int i = 0; i < LastIndex + NumObjectsPerTick; i++)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		if (i == (LastIndex + NumObjectsPerTick))
		{
			LastIndex = i;
		}
	}
	end = FPlatformTime::Seconds();
	/*if ((end - start) >= 0.5f) {
	strife_right = false;
	strife_left = false;
	}*/
	//UE_LOG(LogTemp, Warning, TEXT("code executed in %f seconds."), end - start);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);

}

void ApugrunnerCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	//AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ApugrunnerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	//AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ApugrunnerCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		//Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		//UE_LOG(LogTemp, Warning, TEXT("COSO %s"), *Direction.ToString());
		//UE_LOG(LogTemp, Warning, TEXT("COSO %s"), Value);
		//AddMovementInput(Direction, Value);
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
	}
}

void ApugrunnerCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ApugrunnerCharacter::SwipeLeft()
{
	if (Move_Left_Limit >= 1) {
		if (CanMoveLeftOrRight == true) {
			CanMoveLeftOrRight = false;
			if (Move_Left_Limit >= 2) {
				if (LeftTrace == false) {
					IsMovingLeftOrRight = true;

					const float ValueY = GetActorLocation().Y;
					const float moveValue = ValueY - 250;
					const FVector position = GetActorLocation();

					TEnumAsByte<EMoveComponentAction::Type> enumVal = EMoveComponentAction::Move;
					FLatentActionInfo ActionInfo1;
					ActionInfo1.CallbackTarget = this;
					FRotator TargetRotation = FRotator(0);
					UKismetSystemLibrary::MoveComponentTo(GetCapsuleComponent(), FVector(position.X, moveValue, position.Z),
						TargetRotation, false, false, 0.12f, false, EMoveComponentAction::Move, ActionInfo1);

					Move_Right_Limit = Move_Right_Limit + 1;
					Move_Left_Limit = Move_Left_Limit - 1;
					CanMoveLeftOrRight = true;
					IsMovingLeftOrRight = false;
				}
				else
					Damage();
			}
			else {
				if (LeftTrace == false) {
					IsMovingLeftOrRight = true;

					const float ValueY = GetActorLocation().Y;
					const float moveValue = ValueY - 250;
					const FVector position = GetActorLocation();

					TEnumAsByte<EMoveComponentAction::Type> enumVal = EMoveComponentAction::Move;
					FLatentActionInfo ActionInfo1;
					ActionInfo1.CallbackTarget = this;
					FRotator TargetRotation = FRotator(0);
					UKismetSystemLibrary::MoveComponentTo(GetCapsuleComponent(), FVector(position.X, moveValue, position.Z),
						TargetRotation, false, false, 0.12f, false, EMoveComponentAction::Move, ActionInfo1);

					Move_Right_Limit = Move_Right_Limit + 1;
					Move_Left_Limit = Move_Left_Limit - 1;
					CanMoveLeftOrRight = true;
					IsMovingLeftOrRight = false;
				}
				else
					Damage();
			}
		}
	}
	else
		Damage();
}

void ApugrunnerCharacter::SwipeRight()
{
	if (Move_Right_Limit >= 1) {
		if (CanMoveLeftOrRight == true) {
			CanMoveLeftOrRight = false;
			if (Move_Right_Limit >= 2) {
				if (RightTrace == false) {
					IsMovingLeftOrRight = true;

					const float ValueY = GetActorLocation().Y;
					const float moveValue = ValueY + 250;
					const FVector position = GetActorLocation();

					TEnumAsByte<EMoveComponentAction::Type> enumVal = EMoveComponentAction::Move;
					FLatentActionInfo ActionInfo1;
					ActionInfo1.CallbackTarget = this;
					FRotator TargetRotation = FRotator(0);
					UKismetSystemLibrary::MoveComponentTo(GetCapsuleComponent(), FVector(position.X, moveValue, position.Z),
						TargetRotation, false, false, 0.12f, false, EMoveComponentAction::Move, ActionInfo1);

					Move_Right_Limit = Move_Right_Limit - 1;
					Move_Left_Limit = Move_Left_Limit + 1;
					CanMoveLeftOrRight = true;
					IsMovingLeftOrRight = false;
				}
				else
					Damage();
			}
			else {
				if (RightTrace == false) {
					IsMovingLeftOrRight = true;

					const float ValueY = GetActorLocation().Y;
					const float moveValue = ValueY + 250;
					const FVector position = GetActorLocation();

					TEnumAsByte<EMoveComponentAction::Type> enumVal = EMoveComponentAction::Move;
					FLatentActionInfo ActionInfo1;
					ActionInfo1.CallbackTarget = this;
					FRotator TargetRotation = FRotator(0);
					UKismetSystemLibrary::MoveComponentTo(GetCapsuleComponent(), FVector(position.X, moveValue, position.Z),
						TargetRotation, false, false, 0.12f, false, EMoveComponentAction::Move, ActionInfo1);

					Move_Right_Limit = Move_Right_Limit - 1;
					Move_Left_Limit = Move_Left_Limit + 1;
					CanMoveLeftOrRight = true;
					IsMovingLeftOrRight = false;
				}
				else
					Damage();
			}
		}
	}
	else
		Damage();
}

void ApugrunnerCharacter::SwipeJump() {
	Jump();
}
void ApugrunnerCharacter::SwipeCrouch() {
	FVector tempPosition = GetActorLocation();
	if (tempPosition.Z <= 200.0f) { //120
		DisableInput(PlayerController);
		Crouch();
		//anim montage
		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(UnusedHandle,this, &ApugrunnerCharacter::CrouchDelay, 0.5f, false);
	}
}
void ApugrunnerCharacter::CrouchDelay() {
	EnableInput(PlayerController);
	UnCrouch();
	//correct horizontal postion
}

void Damage() {

}
void Death() {

}
